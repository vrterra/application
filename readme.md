NOTE: This is not the final repository, this is the old repo we used when we first started the project -- trying to create our own engine, which we later stopped and moved to [this](https://bitbucket.org/vrterra/irrlicht) repo.

This repository is left open for those who would want to see all the progress made at all stages. Most of the work in this repository is contained in the develop branch as we didn't get as far as to call anything stable enough to merge into the master branch.

This repository uses CMake to generate project files and to build the project.

The repository also has the following dependencies:

- OpenGL
- GLEW (Windows and Linux)
- SDL2
- (there might be more, I honestly don't remember at this point)
﻿cmake_minimum_required(VERSION 3.2)
project(mani-2016)

if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU" OR "${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Wfatal-errors -Wpedantic -Wunreachable-code -pedantic -std=c++14")
    
    # Also check documentation with clang (if using clang)
    if ("{CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
      set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wdocumentation")
    endif()
    
    # Debug and release flags
    set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS} ${CMAKE_CXX_FLAGS_DEBUG} -g")
    set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS} ${CMAKE_CXX_FLAGS_RELEASE} -O2")
elseif ("${CMAKE_CXX_FLAGS}" STREQUAL "MSVC")
    if (CMAKE_CXX_FLAGS MATCHES "/W[0-4]")
        string(REGEX REPLACE "/W[0-4]" "/W4" CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")
    else()
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /W4")
    endif()
endif()

set(BIN_DIR ${PROJECT_SOURCE_DIR}/bin)

# Let cmake know about our custom Find modules
set(CMAKE_MODULE_PATH  "${CMAKE_SOURCE_DIR}/cmake")

set(PROJECT_SOURCE_DIR "src/")

add_subdirectory(src)
